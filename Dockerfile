FROM openjdk:8u181
RUN wget https://bitbucket.org/alexkasko/openjdk-unofficial-builds/downloads/openjdk-1.7.0-u80-unofficial-linux-amd64-debug-image.zip -O /jdk7.zip && unzip /jdk7.zip -d / && ln -s /openjdk-1.7.0-u80-unofficial-linux-amd64-debug-image /jdk7 && rm -rf /jdk7.zip

