# Run Gradle with openjdk8 but compile your Kotlin/Java classes with openjdk7

Imagine you have a Java/Kotlin library that needs to work on Android. The the library compiles to .jar, hence you don't
really want to use the Android Gradle plugin to compile the library. Thus you use standard Gradle/Maven to compile the library against a standard JDK.
The problem is that typically you compile against JDK8 which contains APIs not present in Android 23 and lower.

It would be great if you could compile and build your Gradle project against JDK7 rt.jar, to make sure you're not using any of the new APIs.
The problem is that the newest Gradle runs on JDK8 only. Attempting to
run Gradle with JDK7 yields:

```
java.lang.UnsupportedClassVersionError: org/jetbrains/kotlin/gradle/plugin/KotlinPluginWrapper : Unsupported major.minor version 52.0
org/jetbrains/kotlin/gradle/plugin/KotlinPluginWrapper : Unsupported major.minor version 52.0
```

(See [KT-27368](https://youtrack.jetbrains.com/issue/KT-27368) for more details).

Not to mention it's pretty hard to download/install JDK7 these days.

The solution is to build your app in a Docker image which contains both JDK8 and JDK7. The JDK8 is used to run Gradle, while we will compile
the project against APIs present in JDK7. This Gitlab repository hosts sources for such a docker image; the docker image itself is published at https://hub.docker.com/r/mvysny/openjdk8_openjdk7

## About

This Docker image is based on [openjdk:8u181](https://hub.docker.com/_/openjdk/) but it includes JDK7 (in the `/jdk7` folder). The JDK7 has been obtained from the [OpenJDK Unofficial Builds Repo](https://github.com/alexkasko/openjdk-unofficial-builds)'s
[openjdk-1.7.0-u80-unofficial-linux-amd64-image.zip](https://bitbucket.org/alexkasko/openjdk-unofficial-builds/downloads/openjdk-1.7.0-u80-unofficial-linux-amd64-image.zip).

This Docker image shows examples on how to:

* Run Gradle on Java 8, but compile your Java or Kotlin project with Java 7, inside the Docker image
* Configure your project to build itself with Java 7
* Configure GitLab CI Continuous Integration Pipeline to build the project with Java 7 and run the tests with Java 7

## Usage With Kotlin-based Projects

To build your Gradle+Kotlin-based project using this image on your local machine, just run this in your terminal:

```
docker run --rm -ti -v /path/to/yourproject:/yourproject mvysny/openjdk8_openjdk7:8u181.7u80 /bin/bash
$ cd /yourproject
$ ./gradlew -Dkotlin.jdkHome=/jdk7
```

You'll need to modify your Kotlin plugin configuration in `build.gradle` a bit:

```groovy
    compileKotlin {
        kotlinOptions {
            jvmTarget = "1.6" // because of Android compatibility
            if (System.getProperty("kotlin.jdkHome") != null) {
                jdkHome = System.getProperty("kotlin.jdkHome")
            }
        }
    }
```

### GitLab Pipeline

Just introduce the following file `.gitlab-ci.yml` into your project:

```yaml
image: mvysny/openjdk8_openjdk7:8u181.7u80
variables:
    GRADLE_OPTS: "-Dorg.gradle.daemon=false"
before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle
  - rm -f  .gradle/caches/modules-2/modules-2.lock
  - rm -fr .gradle/caches/*/plugin-resolution/
cache:
  key: "$CI_COMMIT_REF_NAME"
  paths:
    - .gradle/wrapper/
    - .gradle/caches/
    - .gradle/build-cache/
build:
  script:
    - ./gradlew build -Dkotlin.jdkHome=/jdk7
```

This will make GitLab build your Gradle project automatically in this Docker Image, against JDK7 APIs.

See the [UMN](https://gitlab.com/mvysny/umn) on an example how to do this.

### Compiling App Only Partly In JDK7

Say that the project is a multi-module project which contains a client module (that needs to be Android-compatible), and a server module that is free to use Java 8 features.
You can simply build the whole project using JDK8, and only parts of the app using JDK7 by running Gradle two times:

```
./gradlew build
./gradlew clean umn-proxy-client:build -Dkotlin.jdkHome=/jdk7
```

You can of course configure GitLab Pipeline this way as well:

```yaml
build:
  script:
    - ./gradlew test
    - ./gradlew clean umn-proxy-client:test -Dkotlin.jdkHome=/jdk7
```

## Usage With Java-based Projects

To build your Gradle+Java-based project using this image on your machine, just run the following in your terminal:

```
docker run --rm -ti -v /path/to/yourproject:/yourproject mvysny/openjdk8_openjdk7:8u181.7u80 /bin/bash
$ cd /yourproject
$ ./gradlew -Djava.jdkHome=/jdk7
```

You'll need to modify your Java plugin configuration in `build.gradle` a bit:

```groovy
apply plugin: 'java'
sourceCompatibility = 1.7
targetCompatibility = 1.7
compileJava.options.encoding = 'UTF-8'

if (System.getProperty("java.jdkHome") != null) {
    def javaHome = System.getProperty("java.jdkHome")
    def javaExecutablesPath = new File(javaHome, 'bin')
    def javaExecutables = [:].withDefault { execName ->
        def executable = new File(javaExecutablesPath, execName)
        assert executable.exists(): "There is no ${execName} executable in ${javaExecutablesPath}"
        executable
    }
    tasks.withType(AbstractCompile) {
        options.with {
            fork = true
            forkOptions.javaHome = file(javaHome)
        }
    }
    tasks.withType(Javadoc) {
        executable = javaExecutables.javadoc
    }
    tasks.withType(Test) {
        executable = javaExecutables.java
    }
    tasks.withType(JavaExec) {
        executable = javaExecutables.java
    }
}
```

### GitLab Pipeline

Just introduce the following file `.gitlab-ci.yml` into your project:

```yaml
image: mvysny/openjdk8_openjdk7:8u181.7u80
variables:
    GRADLE_OPTS: "-Dorg.gradle.daemon=false"
before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle
  - rm -f  .gradle/caches/modules-2/modules-2.lock
  - rm -fr .gradle/caches/*/plugin-resolution/
cache:
  key: "$CI_COMMIT_REF_NAME"
  paths:
    - .gradle/wrapper/
    - .gradle/caches/
    - .gradle/build-cache/
build:
  script:
    - ./gradlew -Djava.jdkHome=/jdk7
```

This will make GitLab build your Gradle project automatically in this Docker Image, against JDK7 APIs.

See the [slf4j-handroid](https://gitlab.com/mvysny/slf4j-handroid) on an example how to do this.
